<?php
    require_once('../Models/GenericModel.php');

    /**
    * @method selectDificuldade(integer $id_dificuldade)
    * @method litarDificuldade()
    * @method adicionarDificuldade(string $dsc_dificuldade)
    * @method deletarDificuldade(integer $id_dificuldade)
    * @method atualizarDificuldade(integer $id_dificuldade, string $dsc_dificuldade)
    * @author Jefferson Hillebrecht
    * @author Katia Heckmann
    */
    class DificuldadeModel extends GenericModel
    {
        public function __construct() {
            parent::__construct();
        }
		
        /**
        * Retorna uma dificuldade especifica.
        * @param $id_dificuldade identificador unico
        * @return array com as dificuldades.
        */
		public function selectDificuldade($id_dificuldade)
        {
            return $this->select("SELECT * FROM dificuldade WHERE id_dificuldade = $id_dificuldade;");
        }
		
        /**
        * Retorna uma lista dos niveis de dificuldade.
        * @return array com as dificuldades.
        */
        public function litarDificuldade()
        {
            return $this->select("SELECT * FROM dificuldade;");
        }
		
        /**
        * Adiciona um nivel de dificuldade.
        * @param $dsc_dificuldade descrição da dificuldade (facil, medio, dificil, etc...)
        */
        public function adicionarDificuldade($dsc_dificuldade)
        {
            return $this->select("INSERT INTO dificuldade(dsc_dificuldade) VALUES('$dsc_dificuldade');");
        }

        /**
        * Deleta um nivel de dificuldade.
        * @param $id_dificuldade identificador unico
        */
        public function deletarDificuldade($id_dificuldade)
        {
            return $this->select("DELETE FROM dificuldade WHERE id_dificuldade = $id_dificuldade;");
        }


        /**
        * Atualiza um nivel de dificuldade.
        * @param $id_dificuldade identificador unico
        * @param $dsc_dificuldade descrição da dificuldade (facil, medio, dificil, etc...)
        */
        public function atualizarDificuldade($id_dificuldade, $dsc_dificuldade)
        {
            return $this->select("UPDATE dificuldade SET dsc_dificuldade = '$dsc_dificuldade' WHERE id_dificuldade = $id_dificuldade;");
        }

    }