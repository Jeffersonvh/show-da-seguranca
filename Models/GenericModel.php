<?php
	class GenericModel
	{
		private $strBanco = "mysql";
		private $strHost  = "localhost";
		private $strSchema = "seguranca";
		private $strUser = "root";
		private $strPwd = "";

		private $conn = null;

		public function __construct()
		{
			try{
                $this->conn = new \PDO("$this->strBanco:host=$this->strHost;dbname=$this->strSchema", $this->strUser, $this->strPwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
			}
			catch(\PDOException $e){
				
				if($e->getCode() == PDO_NOT_DATABASE_EXISTS)
					$this->conn = new \PDO("$this->strBanco:host=$this->strHost", $this->strUser, $this->strPwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));					
				else
					die("ERRO: " . $e->getMessage());
			}
		}

		public function __destruct()
		{
			$this->conn = null;
		}
                
                // $banco->select( "select * from usuarios where login = '?' and senha = '?'", ["adm", "123"]);
		public function select($strQuery = null, $param = null)
		{
			if($strQuery == null)
			   return;

			//Preparing query
			$query = $this->conn->prepare($strQuery);

			if(is_array($param) == false && $param !== null)
				$param = array($param);

			for($i = 0; $i < sizeof(array($param)); ++$i){
                $query->bindValue($i + 1, $param[$i]);
              }

			//Execute
			$query->execute();
                        
			//Fetch All
			$rs = $query->fetchAll(PDO::FETCH_ASSOC);
                        
			return $rs;
		}

                public function execute($strQuery = null)
		{
			$query = $this->conn->prepare($strQuery);

			//Executing query
			$query->execute();
		}
	}