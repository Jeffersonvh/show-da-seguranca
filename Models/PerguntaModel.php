<?php
    ini_set('display_startup_errors', 1);
    ini_set('display_errors', 1);
    error_reporting(-1);
    require_once('../Models/GenericModel.php');

    /**
    * @method selectTema(integer $id_tema)
    * @method litarTema()
    * @method adicionarTema(string $dsc_tema)
    * @method deletarDificuldade(integer $id_tema)
    * @method atualizarTema(integer $id_tema, string $dsc_tema)
    * @author Jefferson Hillebrecht
    * @author Katia Heckmann
    */
    class PerguntaModel extends GenericModel
    {
        public function __construct() {
            parent::__construct();
        }
		
        /**
        * Retorna uma pergunta especifico.
        * @param $id_pergunta identificador unico
        * @return array com as perguntas.
        */
		public function selectPergunta($id_pergunta)
        {
            return $this->select("SELECT * FROM pergunta WHERE id_pergunta = $id_pergunta;");
        }
		
        /**
        * Retorna uma lista com todas as perguntas.
        * @return array com as perguntas.
        */
        public function litarPergunta()
        {
            return $this->select("SELECT * FROM pergunta;");
        }
		
        /**
        * Adiciona uma pergunta.
        * @param $dsc_pergunta descrição da pergunta (Pergunta e sí).
        * @param $dsc_motivo motivo da respota certa.
        * @param $id_tema id do tema da pergunta.
        * @param $id_dificuldade id da dificuldade da pergunta.
        */
        public function adicionarPergunta($dsc_pergunta, $dsc_motivo, $id_tema, $id_dificuldade)
        {
            return $this->select("INSERT INTO pergunta(dsc_pergunta, dsc_motivo, id_tema, id_dificuldade) 
                                       VALUES ('$dsc_pergunta',  '$dsc_motivo', $id_tema, $id_dificuldade);");
        }

        /**
        * Deleta uma pergunta.
        * @param $id_pergunta identificador unico
        */
        public function deletarPergunta($id_pergunta)
        {
            return $this->select("DELETE FROM pergunta WHERE id_pergunta = $id_pergunta;");
        }

        /**
        * Atualiza uma pergunta.
        * @param $id_pergunta identificador unico
        * @param $dsc_pergunta descrição da pergunta
        * @param $id_tema
        * @param $id_dificuldade
        */
        public function atualizarPergunta($id_pergunta, $dsc_pergunta, $id_tema, $id_dificuldade)
        {
            return $this->select("UPDATE pergunta 
                                     SET dsc_pergunta = '$dsc_pergunta',
                                         id_tema = $id_tema,
                                         id_dificuldade = $id_dificuldade
                                   WHERE id_tema = $id_tema;");
        }

        /**
        * Retorna uma lista de perguntas em json para o jogo.
        * @param $tema Tema do jogo
        */
        public function buscarPerguntas($tema)
        {
            $filtrar_tema = '';

            if($tema == 1){
                $filtrar_tema = ' AND p.id_tema = 6';
            }else if($tema == 2){
                $filtrar_tema = ' AND p.id_tema = 7';
            }else if($tema == 3){
                $filtrar_tema = ' AND p.id_tema = 8';
            }

            $nivel1 = $this->select("SELECT p.dsc_pergunta, p.dsc_motivo, r.dsc_resposta, r.correta
                                       FROM pergunta p, resposta r
                                      WHERE id_dificuldade = 1 
                                        AND p.id_pergunta = r.id_pergunta
                                        $filtrar_tema
                                      ORDER BY RAND() limit 20");

            $nivel2 = $this->select("SELECT p.dsc_pergunta, p.dsc_motivo, r.dsc_resposta, r.correta
                                       FROM pergunta p, resposta r
                                      WHERE id_dificuldade = 2 
                                        AND p.id_pergunta = r.id_pergunta
                                        $filtrar_tema
                                      ORDER BY RAND() limit 20");

            $nivel3 = $this->select("SELECT p.dsc_pergunta, p.dsc_motivo, r.dsc_resposta, r.correta
                                       FROM pergunta p, resposta r
                                      WHERE id_dificuldade = 3 
                                        AND p.id_pergunta = r.id_pergunta
                                        $filtrar_tema
                                      ORDER BY RAND() limit 24");

            $perguntas = array_merge($nivel1, $nivel2, $nivel3);
            $perguntas_montada;

            $perguntas_montada['user'] = "Dave";
            $c = 0;
            $r = 0;
            $a = 0;
            for($i=0; $i < count($perguntas); $i++){
                if($i != 0){
                    if($perguntas[$i]['dsc_pergunta'] != $perguntas[$i-1]['dsc_pergunta']){
                        $c++;
                        $perguntas_montada['questions'][$c]['text'] = $perguntas[$i]['dsc_pergunta'];
                        $perguntas_montada['questions'][$c]['motivo'] = $perguntas[$i]['dsc_motivo'];
                        $perguntas_montada['questions'][$c]['responses'][$r]['text'] = $perguntas[$i]['dsc_resposta'];
                        $perguntas_montada['questions'][$c]['responses'][$r]['correct'] = $perguntas[$i]['correta'];
                    }else{
                        $perguntas_montada['questions'][$c]['responses'][$r]['text'] = $perguntas[$i]['dsc_resposta'];
                        $perguntas_montada['questions'][$c]['responses'][$r]['correct'] = $perguntas[$i]['correta'];
                    }
                }else{
                    $perguntas_montada['questions'][$c]['text'] = $perguntas[$i]['dsc_pergunta'];
                    $perguntas_montada['questions'][$c]['motivo'] = $perguntas[$i]['dsc_motivo'];
                    $perguntas_montada['questions'][$c]['responses'][$r]['text'] = $perguntas[$i]['dsc_resposta'];
                    $perguntas_montada['questions'][$c]['responses'][$r]['correct'] = $perguntas[$i]['correta'];
                }
                $r++;
            }

            echo(json_encode($perguntas_montada));
        }
    }
