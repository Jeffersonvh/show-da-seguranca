<?php
    require_once('../Models/GenericModel.php');

    /**
    * @method selectTema(integer $id_tema)
    * @method litarTema()
    * @method adicionarTema(string $dsc_tema)
    * @method deletarDificuldade(integer $id_tema)
    * @method atualizarTema(integer $id_tema, string $dsc_tema)
    * @author Jefferson Hillebrecht
    * @author Katia Heckmann
    */
    class RespostaModel extends GenericModel
    {
        public function __construct() {
            parent::__construct();
        }
		
        /**
        * Retorna uma resposta especifico.
        * @param $id_resposta identificador unico
        * @return array com as respostas.
        */
		public function selectResposta($id_resposta)
        {
            return $this->select("SELECT * FROM resposta WHERE id_resposta = $id_resposta;");
        }
		
        /**
        * Retorna uma lista com todas as respostas.
        * @return array com as respostas.
        */
        public function litarResposta()
        {
            return $this->select("SELECT * FROM resposta;");
        }
		
        /**
        * Adiciona uma resposta.
        * @param $dsc_resposta descrição da resposta (resposta e sí).
        * @param $id_pergunta id da pergunta.
        * @param $correta bandeira de resposta certa (true or false).
        */
        public function adicionarResposta($dsc_resposta, $correta, $id_pergunta)
        {
            return $this->select("INSERT INTO resposta(dsc_resposta, correta, id_pergunta) 
                                    VALUES ('$dsc_resposta', $correta, $id_pergunta);");
        }

        /**
        * Deleta uma resposta.
        * @param $id_resposta identificador unico
        */
        public function deletarResposta($id_resposta)
        {
            return $this->select("DELETE FROM resposta WHERE id_resposta = $id_resposta;");
        }

        /**
        * Atualiza uma resposta.
        * @param $id_resposta identificador unico
        * @param $dsc_resposta descrição da resposta (resposta e sí).
        * @param $correta bandeira de resposta certa (true or false).
        */
        public function atualizarResposta($id_resposta, $dsc_resposta, $correta)
        {
            return $this->select("UPDATE resposta 
                                     SET dsc_resposta = '$dsc_resposta',
                                         correta = $correta
                                   WHERE id_resposta = $id_resposta;");
        }

    }
