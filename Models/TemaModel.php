<?php
    require_once('../Models/GenericModel.php');

    /**
    * @method selectTema(integer $id_tema)
    * @method litarTema()
    * @method adicionarTema(string $dsc_tema)
    * @method deletarDificuldade(integer $id_tema)
    * @method atualizarTema(integer $id_tema, string $dsc_tema)
    * @author Jefferson Hillebrecht
    * @author Katia Heckmann
    */
    class TemaModel extends GenericModel
    {
        public function __construct() {
            parent::__construct();
        }
		
        /**
        * Retorna um tema especifico.
        * @param $id_tema identificador unico
        * @return array com os temas.
        */
		public function selectTema($id_tema)
        {
            return $this->select("SELECT * FROM tema WHERE id_tema = $id_tema;");
        }
		
        /**
        * Retorna uma lista dos niveis de tema.
        * @return array com os temas.
        */
        public function litarTema()
        {
            return $this->select("SELECT * FROM tema;");
        }
		
        /**
        * Adiciona um tema.
        * @param $dsc_tema descrição do tema.
        */
        public function adicionarTema($dsc_tema)
        {
            return $this->select("INSERT INTO tema(dsc_tema) VALUES('$dsc_tema');");
        }

        /**
        * Deleta um tema.
        * @param $id_tema identificador unico
        */
        public function deletarTema($id_tema)
        {
            return $this->select("DELETE FROM tema WHERE id_tema = $id_tema;");
        }

        /**
        * Atualiza um nivel de tema.
        * @param $id_tema identificador unico
        * @param $dsc_tema descrição do tema
        */
        public function atualizarTema($id_tema, $dsc_tema)
        {
            return $this->select("UPDATE tema SET dsc_tema = '$dsc_tema' WHERE id_tema = $id_tema;");
        }

    }
