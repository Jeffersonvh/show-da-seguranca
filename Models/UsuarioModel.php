<?php
    require_once('../Models/GenericModel.php');

    /**
    * @method selectDificuldade(integer $id_dificuldade)
    * @method litarDificuldade()
    * @method adicionarDificuldade(string $dsc_dificuldade)
    * @method deletarDificuldade(integer $id_dificuldade)
    * @method atualizarDificuldade(integer $id_dificuldade, string $dsc_dificuldade)
    * @author Jefferson Hillebrecht
    * @author Katia Heckmann
    */
    class UsuarioModel extends GenericModel
    {
        public function __construct() {
            parent::__construct();
        }
		
        /**
        * Retorna uma dificuldade especifica.
        * @param $nome nome usuario
        * @param $senha senha usuario
        * @return usuario.
        */
		public function entrar($nome, $senha)
        {
            return $this->select("SELECT * FROM usuario WHERE nome = '$nome' and senha = '$senha';");
        }

        public function salvarJogo($id_usuario, $pontos)
        {
            return $this->select("INSERT INTO jogo(num_acertos, id_usuario) VALUES($id_usuario, $pontos);");
        }

        public function buscarJogo($id_usuario, $pontos)
        {
            return $this->select("INSERT INTO jogo(num_acertos, id_usuario) VALUES($id_usuario, $pontos);");
        }
    }
