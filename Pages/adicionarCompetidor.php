<?php
//ini_set('display_startup_errors', 1);
//ini_set('display_errors', 1);
//error_reporting(-1);

require_once("../Models/TorneioModel.php");
require_once("../Models/ProprietarioModel.php");
require_once("../Models/PassaroModel.php");

$objmostraTorneioModel = new torneioModel();
$objmostraPassaroModel = new passaroModel();

$id = isset($_GET['id']) ? (int) $_GET['id'] : null;

if (!$id) {
   echo '<center><h2>Ops, não achamos esse torneio. :( <br> <a href="../painel-torneio/index.php">Listar torneios</a></h2>';
   die();
}
$arrTorneios = $objmostraTorneioModel->selectTorneiosById($id);
if($arrTorneios == null){
   echo '<center><h2>Ops, não achamos esse torneio. :( <br> <a href="../painel-torneio/index.php">Listar torneios</a></h2>';
   die();
}
$arrPassaros = $objmostraPassaroModel->litarPassaro();

$arrResultado = $objmostraTorneioModel->mostrarTorneio($id);

$arrResultado2 = $objmostraTorneioModel->mostrarEtapas($id);

$arrResultado3 = $objmostraTorneioModel->etapas($id);

//Organizando saidas iguais

$arrResultadoFiltrado = [];
$arrPassaroFiltrado = [];
$arrResultadoFiltradoEstapas = [];
$intSaidaAtual = null;

foreach ($arrPassaros as $key => $objResultado)
{
	$arrTmp = [$objResultado];

	if(isset($arrPassaros[$key + 1]) && $arrPassaros[$key + 1]['id_passaro'] == $objResultado['id_passaro']) :
		$arrTmp[] = $arrPassaros[$key + 1];
	endif;

	$arrPassaroFiltrado[] = $arrTmp;
}

foreach ($arrResultado as $key => $objResultado)
{
	$arrTmp = [$objResultado];

	if(isset($arrResultado[$key + 1]) && $arrResultado[$key + 1]['id_propretario'] == $objResultado['id_propretario']) :
		$arrTmp[] = $arrResultado[$key + 1];
	endif;

	$arrResultadoFiltrado[] = $arrTmp;
}

foreach ($arrResultado2 as $key => $objResultado)
{
	$arrTmp = [$objResultado];

	if(isset($arrResultado2[$key + 1]) && $arrResultado2[$key + 1]['id_propretario'] == $objResultado['id_propretario']) :
		$arrTmp[] = $arrResultado2[$key + 1];
	endif;

	$arrResultadoFiltradoEstapas[] = $arrTmp;
}
$cont = 1;
//print_r($arrResultadoFiltradoEstapas);
//die();
?>

<html>
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#tabs" ).tabs();
  } );
  </script>
  
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
</head>
<body>

<div class="container">
  <h2><?php  echo $arrTorneios[0]['nome_torneio'];?> - <?php  echo utf8_encode($arrTorneios[0]['data']);?></h2>       
  <br>  
  <div id="tabs">
  <ul>
    <li><a href="#tabs-1">Etapa 1</a></li>
	<li><a href="#tabs-2">Etapa 2</a></li>
	<li><a href="#tabs-3">Etapa 3</a></li>
	<li><a href="#tabs-4">Etapa 4</a></li>
	<li><a href="#tabs-5">Etapa 5</a></li>
    <li><a href="#tabs-6">Classificação Geral</a></li>
  </ul>
  <div id="tabs-1">
	<?php echo $arrResultado3[0]['local']; ?> - <?php echo $arrResultado3[0]['dia']; ?> <input type='hidden' id='etapa1' value="<?php echo $arrResultado3[0]['id_etapa']; ?>">
    <table class="table table-hover">
    <thead>
      <tr>
		<th>#</td>
        <th>Anilha</th>
        <th>Nome Passáro</th>
        <th>Nome Propretário</th>
		<th>Clube</th>
		<th>Cidade</th>
		<th>Primeira Marca</th>
		<th>Pontos</th>
      </tr>
    </thead>
    <tbody>
	<?php foreach ($arrResultadoFiltradoEstapas as $key=>$objResultado) { ?>
		<?php if($objResultado[0]['etapa'] == 1){ ?>
		<tr>
		<td><?php echo $cont++; ?></td>
		<td><?php echo $objResultado[0]['anel']; ?></td>
        <td><?php echo $objResultado[0]['nome_passaro']; ?></td>
        <td><?php echo $objResultado[0]['nome_propretario']; ?></td>
        <td><?php echo $objResultado[0]['associacao']; ?></td>
		<td><?php echo $objResultado[0]['cidade']; ?></td>
		<td><input type="number" size="5" name="prim_marca[]" style="width: 3em;" value="<?php if ($objResultado[0]['prim_marca'] != null){ echo $objResultado[0]['prim_marca']; }else{ echo '-'; } ?>"/>
        <input type="hidden" name="idPontosEtapa[]" value="<?php echo $objResultado[0]['id_pontosEtapa']; ?>">
    </td>
		<td><input type="number" size="5" name="pontos[]" style="width: 3em;" value="<?php if ($objResultado[0]['pontos'] != null){ echo $objResultado[0]['pontos']; }else{ echo '-'; } ?>"/></td>
		</tr>
		<?php
		}else{  $cont = 1; }
		?>
	<?php } ?>
    </tbody>
  </table>
  </div> 
 
  <div id="tabs-2">
	<?php echo $arrResultado3[1]['local']; ?> - <?php echo $arrResultado3[1]['dia']; ?> <input type='hidden' id='etapa2' value="<?php echo $arrResultado3[1]['id_etapa']; ?>">
    <table class="table table-hover">
    <thead>
      <tr>
		<th>#</td>
        <th>Anilha</th>
        <th>Nome Passáro</th>
        <th>Nome Propretário</th>
		<th>Clube</th>
		<th>Cidade</th>
		<th>Primeira Marca</th>
		<th>Pontos</th>
      </tr>
    </thead>
    <tbody>
	<?php foreach ($arrResultadoFiltradoEstapas as $key=>$objResultado) { ?>
		<?php if($objResultado[0]['etapa'] == 2){ ?>
		<tr>
		<td><?php echo $cont++; ?></td>
		<td><?php echo $objResultado[0]['anel']; ?></td>
        <td><?php echo $objResultado[0]['nome_passaro']; ?></td>
        <td><?php echo $objResultado[0]['nome_propretario']; ?></td>
        <td><?php echo $objResultado[0]['associacao']; ?></td>
		<td><?php echo $objResultado[0]['cidade']; ?></td>
		<td><input type="number" size="5" name="prim_marca[]" style="width: 3em;" value="<?php if ($objResultado[0]['prim_marca'] != null){ echo $objResultado[0]['prim_marca']; }else{ echo '-'; } ?>"/>
        <input type="hidden" name="idPontosEtapa[]" value="<?php echo $objResultado[0]['id_pontosEtapa']; ?>">
    </td>
		<td><input type="number" size="5" name="pontos[]" style="width: 3em;" value="<?php if ($objResultado[0]['pontos'] != null){ echo $objResultado[0]['pontos']; }else{ echo '-'; } ?>"/></td>
		</tr>
		<?php
		}else{ $cont = 1; }
		?>
	<?php } ?>
    </tbody>
  </table>
  </div> 
 
   <div id="tabs-3">
	<?php echo $arrResultado3[2]['local']; ?> - <?php echo $arrResultado3[2]['dia']; ?> <input type='hidden' id='etapa3' value="<?php echo $arrResultado3[2]['id_etapa']; ?>">
    <table class="table table-hover">
    <thead>
      <tr>
		<th>#</td>
        <th>Anilha</th>
        <th>Nome Passáro</th>
        <th>Nome Propretário</th>
		<th>Clube</th>
		<th>Cidade</th>
		<th>Primeira Marca</th>
		<th>Pontos</th>
      </tr>
    </thead>
    <tbody>
	<?php foreach ($arrResultadoFiltradoEstapas as $key=>$objResultado) { ?>
		<?php if($objResultado[0]['etapa'] == 3){ ?>
		<tr>
		<td><?php echo $cont++; ?></td>
		<td><?php echo $objResultado[0]['anel']; ?></td>
        <td><?php echo $objResultado[0]['nome_passaro']; ?></td>
        <td><?php echo $objResultado[0]['nome_propretario']; ?></td>
        <td><?php echo $objResultado[0]['associacao']; ?></td>
		<td><?php echo $objResultado[0]['cidade']; ?></td>
		<td><input type="number" size="5" name="prim_marca[]" style="width: 3em;" value="<?php if ($objResultado[0]['prim_marca'] != null){ echo $objResultado[0]['prim_marca']; }else{ echo '-'; } ?>"/>
        <input type="hidden" name="idPontosEtapa[]" value="<?php echo $objResultado[0]['id_pontosEtapa']; ?>">
    </td>
		<td><input type="number" size="5" name="pontos[]" style="width: 3em;" value="<?php if ($objResultado[0]['pontos'] != null){ echo $objResultado[0]['pontos']; }else{ echo '-'; } ?>"/></td>
		</tr>
		<?php
		}else{ $cont = 1; }
		?>
	<?php } ?>
    </tbody>
  </table>
  </div> 
 
   <div id="tabs-4">
	<?php echo utf8_encode($arrResultado3[3]['local']); ?> - <?php echo $arrResultado3[3]['dia']; ?> <input type='hidden' id='etapa4' value="<?php echo $arrResultado3[3]['id_etapa']; ?>">
    <table class="table table-hover">
    <thead>
      <tr>
		<th>#</td>
        <th>Anilha</th>
        <th>Nome Passáro</th>
        <th>Nome Propretário</th>
		<th>Clube</th>
		<th>Cidade</th>
		<th>Primeira Marca</th>
		<th>Pontos</th>
      </tr>
    </thead>
    <tbody>
	<?php foreach ($arrResultadoFiltradoEstapas as $key=>$objResultado) { ?>
		<?php if($objResultado[0]['etapa'] == 4){ ?>
		<tr>
		<td><?php echo $cont++; ?></td>
		<td><?php echo $objResultado[0]['anel']; ?></td>
        <td><?php echo $objResultado[0]['nome_passaro']; ?></td>
        <td><?php echo $objResultado[0]['nome_propretario']; ?></td>
        <td><?php echo $objResultado[0]['associacao']; ?></td>
		<td><?php echo $objResultado[0]['cidade']; ?></td>
		<td><input type="number" size="5" name="prim_marca[]" style="width: 3em;" value="<?php if ($objResultado[0]['prim_marca'] != null){ echo $objResultado[0]['prim_marca']; }else{ echo '-'; } ?>"/>
        <input type="hidden" name="idPontosEtapa[]" value="<?php echo $objResultado[0]['id_pontosEtapa']; ?>">
    </td>
		<td><input type="number" size="5" name="pontos[]" style="width: 3em;" value="<?php if ($objResultado[0]['pontos'] != null){ echo $objResultado[0]['pontos']; }else{ echo '-'; } ?>"/></td>
		</tr>
		<?php
		}else{ $cont = 1; }
		?>
	<?php } ?>
    </tbody>
  </table>
  </div> 
 
   <div id="tabs-5">
   <?php echo $arrResultado3[4]['local']; ?> - <?php echo $arrResultado3[4]['dia']; ?> <input type='hidden' id='etapa5' value="<?php echo $arrResultado3[4]['id_etapa']; ?>">
    <table class="table table-hover">
    <thead>
      <tr>
		<th>#</td>
        <th>Anilha</th>
        <th>Nome Passáro</th>
        <th>Nome Propretário</th>
		<th>Clube</th>
		<th>Cidade</th>
		<th>Primeira Marca</th>
		<th>Pontos</th>
      </tr>
    </thead>
    <tbody>
	<?php foreach ($arrResultadoFiltradoEstapas as $key=>$objResultado) { ?>
		<?php if($objResultado[0]['etapa'] == 5){ ?>
		<tr>
		<td><?php echo $cont++; ?></td>
		<td><?php echo $objResultado[0]['anel']; ?></td>
        <td><?php echo $objResultado[0]['nome_passaro']; ?></td>
        <td><?php echo $objResultado[0]['nome_propretario']; ?></td>
        <td><?php echo $objResultado[0]['associacao']; ?></td>
		<td><?php echo $objResultado[0]['cidade']; ?></td>
		<td><input type="number" size="5" name="prim_marca[]" style="width: 3em;" value="<?php if ($objResultado[0]['prim_marca'] != null){ echo $objResultado[0]['prim_marca']; }else{ echo '-'; } ?>"/>
        <input type="hidden" name="idPontosEtapa[]" value="<?php echo $objResultado[0]['id_pontosEtapa']; ?>">
    </td>
		<td><input type="number" size="5" name="pontos[]" style="width: 3em;" value="<?php if ($objResultado[0]['pontos'] != null){ echo $objResultado[0]['pontos']; }else{ echo '-'; } ?>"/></td>
		</tr>
		<?php
		}else{ $cont = 1; }
		?>
	<?php } ?>
    </tbody>
  </table>
  </div>
  
    <div id="tabs-6">
    <table class="table table-hover">
    <thead>
      <tr>
		<th>#</td>
        <th>Anilha</th>
        <th>Nome Passáro</th>
        <th>Nome Propretário</th>
		<th>Clube</th>
		<th>Cidade</th>
		<th>Total de pontos</th>
      </tr>
    </thead>
    <tbody>
	<?php foreach ($arrResultadoFiltrado as $key=>$objResultado) { ?>
      <tr>
		<td><?php echo $key+1; ?></td>
		<td><?php echo $objResultado[0]['anel']; ?></td>
        <td><?php echo $objResultado[0]['nome_passaro']; ?></td>
        <td><?php echo $objResultado[0]['nome_propretario']; ?></td>
        <td><?php echo $objResultado[0]['associacao']; ?></td>
		<td><?php echo $objResultado[0]['cidade']; ?></td>
		<td><?php if ($objResultado[0]['total'] != null){ echo $objResultado[0]['total']; }else{ echo '-'; } ?></td>
      </tr>
	<?php } ?>
    </tbody>
  </table>
  </div>
  </div>
  <br>
  <button class="btn btn-primary" type="button" onclick="atualizarTorneio();">Atualizar torneio</button>
  <br><br>
  <div>
	<form class="needs-validation" novalidate>
  <div class="form-row">
    <div class="col-md-6 mb-3">
      <label for="nomeTorneio">Anilha</label>
		<select class="form-control js-example-basic-single" id="idProprietario">
			<option>Selecione...</option>
			<?php foreach ($arrPassaroFiltrado as $key=>$objResultado) { ?>
				<option value="<?php echo utf8_encode($objResultado[0]['id_passaro']); ?>"><?php echo utf8_encode($objResultado[0]['anel']); ?></option>
			<?php } ?>
		</select>
    </div>
    <div class="col-md-6 mb-3">
      <label for="data">Nome pássaro</label>
      <input type="text" class="form-control" id="nomePassaro" placeholder="Selecione anilha" disabled>
	  <input type="hidden" id="idPassaro" value="">
    </div>
	
	<div class="col-md-6 mb-3">
      <label for="etapa1">Proprietário</label>
      <input type="text" class="form-control" id="dono" placeholder="Selecione anilha" disabled>
	  <input type="hidden" id="idProprietarioValue" value="">
    </div>
	<div class="col-md-6 mb-3">
      <label for="data">Associação</label>
      <input type="text" class="form-control" id="associacao" placeholder="Selecione anilha" disabled>
	  <input type="hidden" id="idAssociacao" value="">
    </div>
	<div class="col-md-3 mb-3">
	 <button class="btn btn-primary" type="button" onclick="adicionarCompetidor();">Adicionar ao torneio</button>
    </div>
	<div class="col-md-3 mb-3">
	 <p class="the-return"></p>
    </div>
</form>
  </div>  
</div>
<script>
$('select').on('change', function() {
	$.ajax({
		url: '../../View/buscarPropretario.php',
		method: 'post',
		data: {id_passaro: this.value},
		dataType:"json",
		success: function (retorno) {
			document.getElementById("nomePassaro").value = retorno[0]["nome_passaro"];
			document.getElementById("idPassaro").value = retorno[0]["id_passaro"];
			document.getElementById("dono").value = retorno[0]["nome_propretario"];
			document.getElementById("idProprietarioValue").value = retorno[0]["id_propretario"];
			document.getElementById("associacao").value = retorno[0]["associacao"];
		},
		error: function (retorno) {
			document.getElementById("erro").style.display = "block";
		}
	});
})

function adicionarCompetidor() {
  var competidor = {  
          idProprietario: document.getElementById("idProprietario").value,
          idPassaro: document.getElementById("idPassaro").value,
          etapa1: document.getElementById("etapa1").value,
          etapa2: document.getElementById("etapa2").value,
          etapa3: document.getElementById("etapa3").value,
          etapa4: document.getElementById("etapa4").value,
          etapa5: document.getElementById("etapa5").value,
          }
  $.ajax({
    url: '../../View/adicionarCompetidor.php',
    method: 'post',
    data: {competidor: competidor},
    success: function (retorno) {
      alert("Adicionado com sucesso!");
      location.reload();
    },
    error: function (retorno) {
      alert("Ops, algo deu errado. :(");    }
  });
}

function atualizarTorneio() {
  var idPontosEtapa = document.getElementsByName("idPontosEtapa[]");
  var pontos = document.getElementsByName("pontos[]");
  var prim_marca = document.getElementsByName("prim_marca[]");

  var idPontosEtapa2 = [];
  var pontos2 = [];
  var prim_marca2 = [];
  
  for (var i = 0; i < idPontosEtapa.length; i++) {
    idPontosEtapa2[i] = idPontosEtapa[i].value;
    pontos2[i] = pontos[i].value;
    prim_marca2[i] = prim_marca[i].value;
  }
  $.ajax({
    url: '../../View/atualizarPontosEtapas.php',
    method: 'post',
    data: {
      idPontosEtapa: idPontosEtapa2,
      pontos: pontos2,
      prim_marca: prim_marca2
    },
    success: function (retorno) {
      alert("Atualizado com sucesso!");
      location.reload();
    },
    error: function (retorno) {
      alert("Ops, algo deu errado. :(");    }
  });
}
  
  function validaData(date) {
    var datearray = date.split("-");
    var data = datearray[2] + '/' + datearray[1] + '/' + datearray[0];
    return data;
  }
</script>
<script>
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
</body>
</html>
