<?php
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);
?>

<html>
<head>
  <title>Adicionar Tema</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
</head>
<body>

<div class="container">
<br>
<br>
<h2>Adicionar Tema</h2>
<br>

<div class="alert alert-success alert-dismissible" style="display:none;" id="sucesso">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Sucesso!</strong> Tema adicipnado com sucesso.
</div>
<div class="alert alert-danger alert-dismissible" style="display:none;" id="erro">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Erro!</strong> Parece que algo deu errao, atualize a página e tente de novo.
</div>


<form class="needs-validation" novalidate>
  <div class="form-row">
    <div class="col-md-6 mb-3">
      <label for="nomeTorneio">Descrição do tema</label>
      <input type="text" class="form-control" id="dsc_tema" placeholder="Tema" required>
    </div>
  <div class="col-md-6 mb-3">
    </div>
  <button class="btn btn-primary" type="button" onclick="adicionarTema();">Adicionar Tema</button>
</form>
<script>
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
<script>
  (function() {
    'use strict';
    window.addEventListener('load', function() {
      var forms = document.getElementsByClassName('needs-validation');
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
</script>
<script>
  function adicionarTema() {
    $.ajax({
      url: '../View/adicionarTema.php',
      method: 'post',
      data: { dsc_tema: document.getElementById("dsc_tema").value },
      success: function (retorno) {
        document.getElementById("sucesso").style.display = "block";
      },
      error: function (retorno) {
        document.getElementById("erro").style.display = "block";
      }
    });
  }
</script>
</div>
</div>
</body>
</html>
