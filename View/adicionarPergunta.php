<?php

require_once("../Models/PerguntaModel.php");

$dsc_pergunta = $_POST['dsc_pergunta'];
$dsc_motivo = $_POST['dsc_motivo'];
$id_dificuldade = $_POST['id_dificuldade'];
$id_tema = $_POST['id_tema'];

$pergunta = new PerguntaModel();

$pergunta->adicionarPergunta($dsc_pergunta, $dsc_motivo, $id_tema, $id_dificuldade);