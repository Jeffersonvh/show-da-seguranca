<?php

require_once("../Models/PerguntaModel.php");

$tema = $_POST['tema'];

$perguntas = new PerguntaModel();

return $perguntas->buscarPerguntas($tema);