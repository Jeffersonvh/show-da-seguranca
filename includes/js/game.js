var quiz;

var url = new URL(window.location);
var tema = url.searchParams.get("tema");

$.ajax({
  url: '../View/buscarPerguntas.php',
  method: 'post',
  dataType: 'json',
  async: false,
  data: {tema: tema},
  success: function(json) {
    quiz = json;
  }
});

userResponseSkelaton = Array(quiz.questions.length).fill(null);

var app = new Vue({
  el: "#app",
  data: {
    quiz: quiz,
    questionIndex: 0,
    userResponses: userResponseSkelaton,
    isActive: false,
    dismissSecs: 5,
    dismissCountDown: 0 },

  filters: {
    charIndex: function (i) {
      return String.fromCharCode(97 + i);
    } },

  methods: {
    restart: function () {
      this.questionIndex = 0;
      this.userResponses = Array(this.quiz.questions.length).fill(null);
    },
    selectOption: function (index) {
      Vue.set(this.userResponses, this.questionIndex, index);
    },
    next: function (motivo) {
      if (this.questionIndex < this.quiz.questions.length)
      this.questionIndex++;
      alert(motivo)
    },

    prev: function () {
      if (this.quiz.questions.length > 0) this.questionIndex--;
    },
    // Return "true" count in userResponses
    score: function () {
      var score = 0;
      for (let i = 0; i < this.userResponses.length; i++) {
        if (typeof this.quiz.questions[i].responses[this.userResponses[i]] !== "undefined" 
        && this.quiz.questions[i].responses[this.userResponses[i]].correct == 1)
        {
          score = score + 1;
          
        }
      }
      return score;

      //return this.userResponses.filter(function(val) { return val }).length;
    } } });
