-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 04-Jun-2019 às 18:56
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `seguranca`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `dificuldade`
--

CREATE TABLE `dificuldade` (
  `id_dificuldade` int(11) NOT NULL,
  `dsc_dificuldade` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `dificuldade`
--

INSERT INTO `dificuldade` (`id_dificuldade`, `dsc_dificuldade`) VALUES
(3, 'Difícil'),
(1, 'Fácil'),
(2, 'Médio');

-- --------------------------------------------------------

--
-- Estrutura da tabela `jogo`
--

CREATE TABLE `jogo` (
  `id_jogo` int(11) NOT NULL,
  `num_acertos` int(11) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pergunta`
--

CREATE TABLE `pergunta` (
  `id_pergunta` int(11) NOT NULL,
  `dsc_pergunta` varchar(767) NOT NULL,
  `dsc_motivo` varchar(1000) NOT NULL,
  `id_dificuldade` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pergunta`
--

INSERT INTO `pergunta` (`id_pergunta`, `dsc_pergunta`, `dsc_motivo`, `id_dificuldade`, `id_tema`) VALUES
(6, '(Autoria própria) A Engenharia Social é um método de ataque não envolve técnicas de:', 'A resposta correta é Intrusão, porque o ataque utilizando engenharia social foca no contato com a vítima, tentando convencer (persuadir) a vítima fornecer informações confidenciais sem que esta perceba.', 1, 6),
(7, '(Autoria própria) Quais das características da vítima de engenharia social podem ser utilizadas pelo atacante a seu favor para obter informações?', 'A resposta correta é Inocência, boa vontade e confiança, pois o atacante se aproveita dessas características para que a vítima forneça dados confidenciais sem perceber, como, por exemplo, em uma conversa.', 2, 6),
(8, 'Referente a técnicas utilizadas na engenharia social __________ é uma técnica utilizada por engenheiros sociais para _______________.', 'A resposta correta é Shoulder surfing / Observar (Espiar) teclas digitadas, podendo obter informações como a senha de alguma rede social ou banco de vítimas distraídas que acessam aplicativos que podem conter informações confidenciais em público.', 3, 6),
(9, '(Autoria própria) O elo fraco em questão de segurança computacional é ______________.', 'A resposta correta é Indivíduos, pois a política de segurança pode ser a mais forte criada no mundo, se houverem pessoas despreparadas no meio dela as informações podem ser obtidas pelos atacantes.', 1, 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `resposta`
--

CREATE TABLE `resposta` (
  `id_resposta` int(11) NOT NULL,
  `dsc_resposta` varchar(500) NOT NULL,
  `correta` tinyint(1) NOT NULL,
  `id_pergunta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `resposta`
--

INSERT INTO `resposta` (`id_resposta`, `dsc_resposta`, `correta`, `id_pergunta`) VALUES
(1, 'Persuasão', 0, 6),
(2, 'Intrusão', 1, 6),
(3, 'Instigação', 0, 6),
(4, 'Ingenuidade, boa vontade e perspicácia', 0, 7),
(5, 'Inocência, boa vontade e confiança', 1, 7),
(6, 'Ingenuidade, lucidez e boa vontade', 0, 7),
(7, 'Head surfing / Espiar teclas digitadas', 0, 8),
(8, 'Shoulder surfing / Observar teclas digitadas', 1, 8),
(9, 'Ear surfing / Ouvir conversar confidenciais', 0, 8),
(10, 'Indivíduos', 1, 9),
(11, 'Invasores', 0, 9),
(12, 'Insegurança', 0, 9);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tema`
--

CREATE TABLE `tema` (
  `id_tema` int(11) NOT NULL,
  `dsc_tema` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tema`
--

INSERT INTO `tema` (`id_tema`, `dsc_tema`) VALUES
(8, 'Criptografia'),
(6, 'Golpes & Ataques'),
(7, 'Segurança da Informação');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nome`, `senha`) VALUES
(1, 'jeffe', 'jeffe');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dificuldade`
--
ALTER TABLE `dificuldade`
  ADD PRIMARY KEY (`id_dificuldade`),
  ADD UNIQUE KEY `dsc_dificuldade` (`dsc_dificuldade`);

--
-- Indexes for table `jogo`
--
ALTER TABLE `jogo`
  ADD PRIMARY KEY (`id_jogo`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indexes for table `pergunta`
--
ALTER TABLE `pergunta`
  ADD PRIMARY KEY (`id_pergunta`),
  ADD UNIQUE KEY `dsc_pergunta` (`dsc_pergunta`),
  ADD KEY `id_dificuldade` (`id_dificuldade`),
  ADD KEY `id_tema` (`id_tema`);

--
-- Indexes for table `resposta`
--
ALTER TABLE `resposta`
  ADD PRIMARY KEY (`id_resposta`),
  ADD UNIQUE KEY `dsc_resposta` (`dsc_resposta`),
  ADD KEY `id_pergunta` (`id_pergunta`);

--
-- Indexes for table `tema`
--
ALTER TABLE `tema`
  ADD PRIMARY KEY (`id_tema`),
  ADD UNIQUE KEY `dsc_tema` (`dsc_tema`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `nome` (`nome`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dificuldade`
--
ALTER TABLE `dificuldade`
  MODIFY `id_dificuldade` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jogo`
--
ALTER TABLE `jogo`
  MODIFY `id_jogo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pergunta`
--
ALTER TABLE `pergunta`
  MODIFY `id_pergunta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `resposta`
--
ALTER TABLE `resposta`
  MODIFY `id_resposta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tema`
--
ALTER TABLE `tema`
  MODIFY `id_tema` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `jogo`
--
ALTER TABLE `jogo`
  ADD CONSTRAINT `jogo_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `pergunta`
--
ALTER TABLE `pergunta`
  ADD CONSTRAINT `pergunta_ibfk_1` FOREIGN KEY (`id_dificuldade`) REFERENCES `dificuldade` (`id_dificuldade`) ON DELETE CASCADE,
  ADD CONSTRAINT `pergunta_ibfk_2` FOREIGN KEY (`id_tema`) REFERENCES `tema` (`id_tema`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `resposta`
--
ALTER TABLE `resposta`
  ADD CONSTRAINT `resposta_ibfk_1` FOREIGN KEY (`id_pergunta`) REFERENCES `pergunta` (`id_pergunta`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
